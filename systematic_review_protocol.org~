#+TITLE:  Systematic review protocol
#+AUTHOR: Miguel Felipe Silva Vasconcelos
#+DATE:   11-03-2021

#+LANGUAGE: en
# #+PROPERTY: header-args :eval never-export

#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>

* Title

Scheduling for green clouds: an overview 

* Objectives:
  
Identify and analyze the existing scheduling algorithms and approaches
with the objective to reduce the non-renewable energy consumption in
cloud computing platforms.


* Question formularization
** Research Questions:

- What scheduling algorithms and methods are used to reduce the overall energy consumption
  and increase the renewable energy usage in cloud computing platforms?
- Which of these techniques considers energy storage devices?
- For the execution of the experiments, was some simulator used?
  If so, which one?
- Which database was used for the execution of the experiments? 
- Which metrics were used to evaluate the proposed solution?
- What are the baselines used to evaluate the proposed solution?


- *Intervention (what is going to be observed)*: scheduling algorithms and techniques

- *Control*: articles, theses, and dissertations obtained with the advisor or on the internet through scientific databases in the field of computing, previous systematic reviews

  - B. Camus, F. Dufossé, and A. Orgerie.   A stochastic approach for
    optimizing green energy consumption in distributed clouds. In
    SMARTGREENS 2017 - International Conference on SmartCities and
    Green ICT Systems, Porto, Portugal, 2017. SMARTGREENS.

  - B. Camus, A. Blavette, F. Dufossé, and
    A. Orgerie. Self-Consumption Optimization of Renewable Energy
    Production in Distributed Clouds.  In 2018 IEEE International
    Conference on Cluster Computing (CLUSTER), pages 370–380, Belfast,
    UK, 2018. IEEE.

  -  B. Camus, F. Dufossé, A. Blavette, M. Quinson, and
    A. Orgerie. Network-Aware Energy-Efficient Virtual Machine
    Management in Distributed Cloud Infrastructures with On-Site
    Photovoltaic Production. In 2018 30th International Symposium on
    Computer Architecture and High Performance Computing (SBAC-PAD),
    pages 86–92, Lyon, France, 2018. IEEE.

- *Population (target group that is going to be observed by the intervention)*: projects that propose scheduling strategies for cloud computing platforms to reduce non-renewable energy consumption

- *Results*: a deep and comprehensive view of scheduling algorithms and techniques to reduce non-renewable energy consumption on cloud computing platforms

- *Application (who is going to be benefited by the review)*: researches in the area of green computing, cloud computing providers aiming to reduce the environmental impact
  
  
* Sources Selection:

** Sources Selection Criteria Definition

Available online, in scientific databases in the area of computer
science, or in other media, respecting the Systematic Review requirements.


** Search Strings:
   <<sec:search-string>>
   #+begin_src 
    (energy-aware OR green energy OR renewable energy) AND
    (scheduling  OR scheduler or optimization) AND 
    (cloud OR clouds OR datacenter OR datacentre OR 
         data center OR data centre OR datacenters OR
          datacentres OR data centers OR data centres)   
   #+end_src


*** IEE


*** ACM

*** SCOPUS


    

** Sources Lists
 <<sec:sources>>
- [[http://ieeexplore.ieee.org/Xplore/][IEEE digital library]]
- [[https://dl.acm.org/][ACM Digital Library]]
- [[https://www.scopus.com/home.uri][Scopus Preview]]


** Type of the articles:

Articles that propose scheduling algorithms and techniques for reducing the consumption of non-renewable energy on cloud computing platforms.


** Studies Languages:

English


* Studies Selection
  
** Studies Inclusion and Exclusion Criteria Definition:
   <<sec:inclusion-exclusion-criteria>>
*** Inclusion:
1. Published and available in full in scientific databases or printed versions
2. Recent (published from 2016)
3. Propose scheduling techniques to reduce non-renewable energy consumption on cloud computing platforms
4. Systematic reviews on reducing non-renewable energy on cloud computing platforms

*** Exclusion:
1. Focus on reducing the consumption of non-renewable energy with other strategies except the scaling of virtual machines
2. Do not consider the use of renewable energy
3. Only use cloud computing platforms
4. Present scheduling techniques for other purposes
5. Do not consider cloud computing platforms
6. Do not focus on minimizing the consumption of non-renewable energy


** Procedures for Studies Selection

The string presented in section [[sec:search-string]] will be submitted to the search
engines of the sources listed on section [[sec:sources]] (adapted to each
one). The following information of the articles will considered for the search: title, abstract and keywords.

In order to apply the inclusion and exclusion criteria, the reviewer
will examine the information of the title, abstract, introduction and conclusion. If those are not sufficient, the full text of the article will be
examined.

An article will proceed to the quality evaluation if it presents at
least 3 inclusion criteria (to consider both 
systematic reviews and articles that proposes new methods) and no
exclusion criteria.

 
** Studies Quality Evaluation
   
*** Studies Quality Criteria Definition:

To assess the quality of the articles that were selected using the
inclusion and exclusion criteria, the following score is given:

1. Journal/conference/workshop: [1 | 0,5 | 0]  
3. Algorithm is online: [1 | 0]
4. Considers geographically distributed data centers: [1 | 0]
5. Considers energy storage systems/devices: [1 | 0]
6. Uses real workload databases: [1 | 0]
7. Uses real databases of renewable energy generation: [1 | 0]
8. Uses a framework validated by the scientific community to develop the simulations: [1 | 0]
9. Uses more than one baseline system to evaluate the proposed solution: [1 | 0]

The review will first collect information from the abstract, introduction, methodology, results, and conclusion to calculate the quality score. If necessary, the entire article will be read. Furthermore, the article's quality is also evaluated based on where it was published (criteria 1 and 2).

Articles that reach a score equal to or greater than 5.5 points will be considered as with satisfactory quality ad will be included on the present systematic review.


* Information Extraction

The articles selected after the quality evaluation will be fully read
by the reviewer, who will summarize each one and fill data extraction
forms that will have the following fields:

- Title of work
- Source
- DOI
- Year
- Summary
- Algorithm description
- Databases 
- Baselines 
- Results

The reviewer can also provide his opinion about the article, highlighting its strong and weak aspects.

* Data synthesis:
  
A report will be developed after the extraction phase containing a quantitative and qualitative analysis of the study.

The quantitative analyses will present information concerning the distribution of the different algorithms, the number of articles published by year and country, baselines used, databases used, energy storage systems considered, type of renewable energy considered.

Regarding the qualitative analysis, a table in the form of a checklist
about the studies' features  will be presented.


* Testing of the systematic review

In order to validate the systematic review protocol, a test was
conducted to try to obtain the control articles. The selected database
was SCOPUS, and the following search string was used:

TITLE-ABS-KEY ( ( "energy-aware"  OR  "green energy"  OR  "renewable energy" )  AND  ( "scheduling"  OR  "scheduler"  OR  "optimization" )  AND  ( "cloud"  OR  "clouds"  OR  datacenter  OR  datacentre  OR  "data center"  OR  "data centre"  OR  datacenters  OR  datacentres  OR  "data centers"  OR  "data centres" ) )  AND  ( LIMIT-TO ( PREFNAMEAUID ,  "Camus, B.#55668550300" ) )  



The search was executed on 11/03/2021. The filter ````LIMIT-TO(
PREFNAMEAUID,"Camus, B.#55668550300")```` was used to select the
articles of the author "Benjamin Camus". Since he is the first author
of the control articles, this filter was added to reduce the number of
the results for the purpose of the validation. On the real search for the systematic review, this filter will not be present.


Results:

#+begin_src 
﻿Authors,Author(s) ID,Title,Year,Source title,Volume,Issue,Art. No.,Page start,Page end,Page count,Cited by,DOI,Link,Affiliations,Authors with affiliations,Abstract,Author Keywords,Index Keywords,Document Type,Publication Stage,Open Access,Source,EID
"Gougeon A., Camus B., Orgerie A.-C.","57219752215;55668550300;25959229000;","Optimizing green energy consumption of fog computing architectures",2020,"Proceedings - Symposium on Computer Architecture and High Performance Computing","2020-September",, 9235038,"75","82",,,"10.1109/SBAC-PAD49847.2020.00021","https://www.scopus.com/inward/record.uri?eid=2-s2.0-85095863717&doi=10.1109%2fSBAC-PAD49847.2020.00021&partnerID=40&md5=07020ce1d59e65ba3293d07455a24e13","Univ. Rennes, Inria, CNRS, !!!IRISA, @@@ENS Rennes, France","Gougeon, A., Univ. Rennes, Inria, CNRS, !!!IRISA, @@@ENS Rennes, France; Camus, B., Univ. Rennes, Inria, CNRS, !!!IRISA, @@@ENS Rennes, France; Orgerie, A.-C., Univ. Rennes, Inria, CNRS, !!!IRISA, @@@ENS Rennes, France","The Cloud already represents an important part of the global energy consumption, and this consumption keeps increasing. Many solutions have been investigated to increase its energy efficiency and to reduce its environmental impact. However, with the introduction of new requirements, notably in terms of latency, an architecture complementary to the Cloud is emerging: the Fog. The Fog computing paradigm represents a distributed architecture closer to the end-user. Its necessity and feasibility keep being demonstrated in recent works. However, its impact on energy consumption is often neglected and the integration of renewable energy has not been considered yet. The goal of this work is to exhibit an energy-efficient Fog architecture considering the integration of renewable energy. We explore three resource allocation algorithms and three consolidation policies. Our simulation results, based on real traces, show that the intrinsic low computing capability of the nodes in a Fog context makes it harder to exploit renewable energy. In addition, the share of the consumption from the communication network between the computing resources increases in this context, and the communication devices are even harder to power through renewable sources. © 2020 IEEE.","Fog computing; Job scheduling; Renewable energy","Computer architecture; Energy efficiency; Energy policy; Energy utilization; Environmental impact; Fog; Green computing; Network architecture; Communication device; Computing architecture; Computing capability; Computing resource; Distributed architecture; Integration of renewable energies; Renewable energies; Resource allocation algorithms; Fog computing",Conference Paper,"Final","",Scopus,2-s2.0-85095863717
"Camus B., Dufosse F., Blavette A., Quinson M., Orgerie A.-C.","55668550300;35174284900;54894544200;23393787800;25959229000;","Network-Aware Energy-Efficient Virtual Machine Management in Distributed Cloud Infrastructures with On-Site Photovoltaic Production",2019,"Proceedings - 2018 30th International Symposium on Computer Architecture and High Performance Computing, SBAC-PAD 2018",,, 8645901,"86","92",,1,"10.1109/CAHPC.2018.8645901","https://www.scopus.com/inward/record.uri?eid=2-s2.0-85063126503&doi=10.1109%2fCAHPC.2018.8645901&partnerID=40&md5=dc8d75a71ab29760bf3c35543324dbc9","Inria, CNRS, IRISA, Univ. Rennes, Rennes, France; LIG, Inria, Grenoble, France; CNRS, SATIE, Univ. Rennes, Rennes, France","Camus, B., Inria, CNRS, IRISA, Univ. Rennes, Rennes, France; Dufosse, F., LIG, Inria, Grenoble, France; Blavette, A., CNRS, SATIE, Univ. Rennes, Rennes, France; Quinson, M., Inria, CNRS, IRISA, Univ. Rennes, Rennes, France; Orgerie, A.-C., Inria, CNRS, IRISA, Univ. Rennes, Rennes, France","Distributed Clouds are nowadays an essential component for providing Internet services to always more numerous connected devices. This growth leads the energy consumption of these distributed infrastructures to be a worrying environmental and economic concern. In order to reduce energy costs and carbon footprint, Cloud providers could resort to producing onsite renewable energy, with solar panels for instance. In this paper, we propose NEMESIS: a Network-aware Energy-efficient Management framework for distributEd cloudS Infrastructures with on-Site photovoltaic production. NEMESIS optimizes VM placement and balances VM migration and green energy consumption in Cloud infrastructure embedding geographically distributed data centers with on-site photovoltaic power supply. We use the Simgrid simulation toolbox to evaluate the energy efficiency of NEMESIS against state-of-the-art approaches. © 2018 IEEE.","consolidation; Distributed cloud computing; energy-efficient scheduling; network-aware migration; on-site production; renewable energy","Carbon footprint; Computer architecture; Consolidation; Distributed computer systems; Energy policy; Energy utilization; Green computing; Network architecture; Photovoltaic cells; Virtual machine; Distributed clouds; Energy-Efficient Scheduling; Network-aware; On-site production; Renewable energies; Energy efficiency",Conference Paper,"Final","",Scopus,2-s2.0-85063126503
"Camus B., Dufossé F., Orgerie A.-C.","55668550300;35174284900;25959229000;","The SAGITTA Approach for Optimizing Solar Energy Consumption in Distributed Clouds with Stochastic Modeling",2019,"Communications in Computer and Information Science","921",,,"52","76",,1,"10.1007/978-3-030-02907-4_3","https://www.scopus.com/inward/record.uri?eid=2-s2.0-85057566319&doi=10.1007%2f978-3-030-02907-4_3&partnerID=40&md5=d2c17e49f94b2e036ab3a136ccc61cfa","Inria, IRISA, Rennes, France; Inria, CRIStAL, Lille, France; CNRS, IRISA, Rennes, France","Camus, B., Inria, IRISA, Rennes, France; Dufossé, F., Inria, CRIStAL, Lille, France; Orgerie, A.-C., CNRS, IRISA, Rennes, France","Facing the urgent need to decrease data centers’ energy consumption, Cloud providers resort to on-site renewable energy production. Solar energy can thus be used to power data centers. Yet this energy production is intrinsically fluctuating over time and depending on the geographical location. In this paper, we propose a stochastic modeling for optimizing solar energy consumption in distributed clouds. Our approach, named SAGITTA (Stochastic Approach for Green consumption In disTributed daTA centers), is shown to produce a virtual machine scheduling close to the optimal algorithm in terms of energy savings and to outperform classical round-robin approaches over varying Cloud workloads and real solar energy generation traces. © 2019, Springer Nature Switzerland AG.","Data centers; Distributed clouds; Energy efficiency; On/Off techniques; Renewable energy; Scheduling","Energy conservation; Energy efficiency; Energy utilization; Intelligent systems; Intelligent vehicle highway systems; Scheduling; Smart city; Solar energy; Stochastic systems; Traffic control; Data centers; Distributed clouds; Energy generations; Geographical locations; On/Off techniques; Renewable energies; Stochastic approach; Virtual machine scheduling; Green computing",Conference Paper,"Final","",Scopus,2-s2.0-85057566319
"Camus B., Blavette A., Dufosse F., Orgerie A.-C.","55668550300;54894544200;35174284900;25959229000;","Self-Consumption Optimization of Renewable Energy Production in Distributed Clouds",2018,"Proceedings - IEEE International Conference on Cluster Computing, ICCC","2018-September",, 8514897,"370","380",,2,"10.1109/CLUSTER.2018.00055","https://www.scopus.com/inward/record.uri?eid=2-s2.0-85057243396&doi=10.1109%2fCLUSTER.2018.00055&partnerID=40&md5=c7dca04d6b725a7646e736467a5b2ec8","Univ. Rennes, Inria, CNRS- IRISA, Rennes, France; Univ. Rennes, CNRS, SATIE, Rennes, France; Univ. Grenoble Alpes, CNRS, Inria, Grenoble INP, LIG, Grenoble, 38000, France","Camus, B., Univ. Rennes, Inria, CNRS- IRISA, Rennes, France; Blavette, A., Univ. Rennes, CNRS, SATIE, Rennes, France; Dufosse, F., Univ. Grenoble Alpes, CNRS, Inria, Grenoble INP, LIG, Grenoble, 38000, France; Orgerie, A.-C., Univ. Rennes, Inria, CNRS- IRISA, Rennes, France","The growing appetite of new technologies, such as Internet-of-Things, for Cloud resources leads to an unprecedented energy consumption for these infrastructures. In order to make these energy-hungry distributed systems more sustainable, Cloud providers resort more and more to on-site renewable energy production facilities like photovoltaic panels. Yet, this intermittent and variable electricity production is often uncorrelated with the Cloud consumption induced by its workload. Geographical load balancing, virtual machine (VM) migration and consolidation can be used to exploit multiple Cloud data centers' locations and their associated photovoltaic panels for increasing their renewable energy consumption. However, these techniques cost energy and network bandwidth, and this limits their utilization. In this paper, we propose to rely on the flexibility brought by Smart Grids to exchange renewable energy between distributed sites and thus, to further increase the overall Cloud's self-consumption of the locally-produced renewable energy. Our solution is named SCORPIUS: Self-Consumption Optimization of Renewable energy Production In distribUted cloudS. It takes into account telecommunication network constraints and electrical grid requirements to optimize the Cloud's self-consumption by trading-off between VM migration and renewable energy exchange. Our simulation-based results show that SCORPIUS outperforms existing solutions on various workload traces of production Clouds in terms of both renewable self-consumption and overall energy consumption. © 2018 IEEE.","Distributed cloud computing; Energy-efficient consolidation; Renewable energy; Self-consumption; Smart Grids","Balancing; Cluster computing; Computer architecture; Electric power transmission networks; Energy efficiency; Energy utilization; Green computing; Photovoltaic cells; Virtual machine; Distributed clouds; Energy efficient; Renewable energies; Self- consumption; Smart grid; Smart power grids",Conference Paper,"Final","",Scopus,2-s2.0-85057243396
"Camus B., Dufossé F., Orgerie A.-C.","55668550300;35174284900;25959229000;","A stochastic approach for optimizing green energy consumption in distributed clouds",2017,"SMARTGREENS 2017 - Proceedings of the 6th International Conference on Smart Cities and Green ICT Systems",,,,"47","59",,7,"10.5220/0006306500470059","https://www.scopus.com/inward/record.uri?eid=2-s2.0-85025477103&doi=10.5220%2f0006306500470059&partnerID=40&md5=17b46a9e341107da88d1bb2402081a03","Inria, IRISA, Rennes, France; Inria, CRIStAL, Lille, France; CNRS, IRISA, Rennes, France","Camus, B., Inria, IRISA, Rennes, France; Dufossé, F., Inria, CRIStAL, Lille, France; Orgerie, A.-C., CNRS, IRISA, Rennes, France","The energy drawn by Cloud data centers is reaching worrying levels, thus inciting providers to install on-site green energy producers, such as photovoltaic panels. Considering distributed Clouds, workload managers need to geographically allocate virtual machines according to the green production in order not to waste energy. In this paper, we propose SAGITTA: A Stochastic Approach for Green consumption In disTributed daTA centers. We show that compared to the optimal solution, SAGITTA consumes 4% more brown energy, and wastes only 3.14% of the available green energy, while a traditional round-robin solution consumes 14.4% more energy overall than optimum, and wastes 28.83% of the available green energy. © 2017 by SCITEPRESS Science and Technology Publications, Lda. All Rights Reserved.","Data centers; Distributed clouds; Energy efficiency; On/off techniques; Renewable energy; Scheduling","Energy efficiency; Energy policy; Energy utilization; Photovoltaic cells; Scheduling; Smart city; Stochastic systems; Cloud data centers; Data centers; Distributed clouds; On/off techniques; Optimal solutions; Photovoltaic panels; Renewable energies; Stochastic approach; Green computing",Conference Paper,"Final","",Scopus,2-s2.0-85025477103
#+end_src

As we can see, the control articles were successfully returned in this search.

